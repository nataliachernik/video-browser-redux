import {combineReducers} from 'redux';

import Videos from './videos';
import SelectedVideo from './selectedVideo';

const rootReducer = combineReducers({
    videos: Videos,
    selectedVideo: SelectedVideo
});

export default rootReducer;