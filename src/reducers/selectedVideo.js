import {UPDATE_VIDEOS} from './videos';

export const SELECT_VIDEO = 'SELECT_VIDEO';

export const Video = (state = null, action) => {
    switch (action.type) {
        case UPDATE_VIDEOS:
            return action.videos[0];
        case SELECT_VIDEO:
            return action.selectedVideo;
        default:
            return state;
    }
};

export default Video;