import YTSearch from 'youtube-api-search';

import {UPDATE_VIDEOS} from '../reducers/videos';
import {SELECT_VIDEO} from '../reducers/selectedVideo';

const API_KEY = 'AIzaSyC2Q009iTntdBOhh62psOlD-TQvjzjbRq8';

export const fetchVideos = (term) => (dispatch, getState) => {
    YTSearch({key: API_KEY, term}, videos => {
        dispatch({type: UPDATE_VIDEOS, videos: videos});
    });
};

export const selectVideo = (selectedVideo) => ({
    type: SELECT_VIDEO,
    selectedVideo: selectedVideo
});