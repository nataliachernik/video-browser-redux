import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import VideoListItem from './VideoListItem';
import {selectVideo} from '../actions/actions';

class VideoList extends Component {
    render() {
        const videoItems = this.props.videos.map(video => {
            return (
                <VideoListItem
                    key={video.etag}
                    video={video}
                    onVideoSelect={this.props.selectVideo}
                />
            );
        });

        return (
            <ul className="list-group col-md-4">
                {videoItems}
            </ul>
        );
    }
}

function mapStateToProps(state) {
    return {
        videos: state.videos
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({selectVideo}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoList);