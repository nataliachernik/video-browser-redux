import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import SearchBar from './SearchBar';
import VideoDetails from './VideoDetails';
import VideoList from './VideoList';

import {fetchVideos} from '../actions/actions';

import './App.css';

const DEFAULT_TERM = 'surfboards';

class App extends Component {
    componentDidMount() {
        this.props.fetchVideos(DEFAULT_TERM);
    }

    render() {
        return (
            <div className="container">
                <SearchBar />
                <div className="row">
                    <VideoDetails />
                    <VideoList />
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchVideos}, dispatch);
}

export default connect(null, mapDispatchToProps)(App);