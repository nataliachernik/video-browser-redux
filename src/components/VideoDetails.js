import React, {Component} from 'react';
import {connect} from 'react-redux';

class VideoDetails extends Component {
    render() {
        if (this.props.selectedVideo) {
            return this.renderVideoDetails();
        } else {
            return this.renderLoader();
        }
    }

    renderVideoDetails() {
        const {selectedVideo} = this.props;

        const videoId = selectedVideo.id.videoId;
        const url = `https://www.youtube.com/embed/${videoId}`;

        return (
            <div className="col-md-8">
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src={url} title="youtube video" />
                </div>
                <div className="details">
                    <h5>{selectedVideo.snippet.title}</h5>
                    <div>{selectedVideo.snippet.description}</div>
                </div>
            </div>
        );
    }

    renderLoader() {
        return <div>Loading...</div>;
    }
}

function mapStateToProps(state) {
    return {
        selectedVideo: state.selectedVideo
    };
}

export default connect(mapStateToProps)(VideoDetails);