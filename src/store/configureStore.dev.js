import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import rootReducer from '../reducers/reducer';
import DevTools from '../root/DevTools';

const configureStore = preloadedState => createStore(
    rootReducer,
    preloadedState,
    compose(
        applyMiddleware(thunk, createLogger()),
        DevTools.instrument()
    )
);

export default configureStore;